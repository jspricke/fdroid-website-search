#!/usr/bin/env python3
#
# fdroidfetchindex.py - part of fdroid-website-search django application
# Copyright (C) 2017-2019 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.db import connection, transaction

from fdroid_website_search.templatetags import fdroid_website_search_tags

from fdroid_website_search.models import Language
from fdroid_website_search.models import FDroidSite
from fdroid_website_search.models import App
from fdroid_website_search.models import AppI18n
from fdroid_website_search.models import Category
from fdroid_website_search.models import AntiFeature
from fdroid_website_search.models import License

from collections import defaultdict

import os
import json
import html
import zipfile
import tempfile
import textwrap
import urllib.request
import datetime
import itertools
import collections


def batch(iterable, size):
    def ticker(x, s=size, a=[-1]):
        r = a[0] = a[0] + 1
        return r // s
    for k, g in itertools.groupby(iterable, ticker):
         yield g


class Command(BaseCommand):
    def __init__(self):
        self._lang_cache = {}
        self._site_cache = {}
        self._category_cache = {}
        self._antifeature_cache = {}
        self._license_cache = {}

    def handle(self, *args, **options):

        def get_or_create_lang(name):
            if name in self._lang_cache:
                return self._lang_cache[name]
            l = Language.objects.filter(name=name).first()
            if not l:
                l = Language()
                l.name = name
                l.save()
                self._lang_cache[name] = l
            return l

        def get_or_create_site(repo, site, iconMirror):
            if site in self._site_cache:
                return self._site_cache[site]
            s = FDroidSite.objects.filter(repoUrl=repo, siteUrl=site).first()
            if not s:
                s = FDroidSite()
                s.repoUrl = repo
                s.siteUrl = site
                s.iconMirrorUrl = iconMirror
                s.save()
                self._site_cache[site] = s
            return s

        def get_or_create_category(name):
            if name in self._category_cache:
                return self._category_cache[name]
            c = Category.objects.filter(name=name).first()
            if not c:
                c = Category()
                c.name = name
                c.save()
                self._category_cache[name] = c
            return c

        def get_or_create_antifeature(name):
            if name in self._antifeature_cache:
                return self._antifeature_cache[name]
            af = AntiFeature.objects.filter(name=name).first()
            if not af:
                af = AntiFeature()
                af.name = name
                af.save()
                self._antifeature_cache[name] = af
            return af

        def get_or_create_license(name):
            if name in self._license_cache:
                return self._license_cache[name]
            l = License.objects.filter(name=name).first()
            if not l:
                l = License()
                l.name = name
                l.save()
                self._license_cache[name] = l
            return l

        for fdroid_site_info in fdroid_website_search_tags.get_all_repos():
            fdroid_site = get_or_create_site(fdroid_site_info['repo'],
                                             fdroid_site_info['site'],
                                             fdroid_site_info['icon-mirror'])

            with tempfile.TemporaryDirectory() as tmpdir:
                jarpath = os.path.join(tmpdir, 'index-v1.jar')
                index_url = fdroid_site.repoUrl + '/index-v1.jar'
                print("fetching '{}' ...".format(index_url))
                with urllib.request.urlopen(index_url) as i:
                    with open(jarpath, 'wb') as o:
                        o.write(i.read())
                with zipfile.ZipFile(jarpath) as z:
                    index = json.loads(z.read('index-v1.json').decode('utf-8'))

                fdroid_site.lastUpdated = datetime.datetime.now(datetime.timezone.utc)
                fdroid_site.repoTimestamp = datetime.datetime.fromtimestamp(index['repo']['timestamp']/1000., datetime.timezone.utc)
                fdroid_site.save()

                print("updating database ...")
                for appbatch in batch(index['apps'], 100):
                    batch_create = []
                    batch_update = []
                    for app in appbatch:
                        try:
                            a = App.objects.get(packageName=app['packageName'])
                            batch_update.append(a)
                        except ObjectDoesNotExist:
                            a = App()
                            batch_create.append(a)
                        a.name = html.unescape(app.get('name', ''))
                        a.packageName = app.get('packageName', '')
                        a.summary = html.unescape(app.get('summary', ''))
                        a.description = html.unescape(app.get('description', ''))
                        a.whatsNew = html.unescape(app.get('whatsNew', ''))

                        a.authorName = html.unescape(app.get('authorName', ''))
                        a.authorEmail = html.unescape(app.get('authorEmail', ''))

                        a.license = get_or_create_license(app.get('license', None))

                        a.webSite = app.get('webSite', '')
                        a.sourceCode = app.get('sourceCode', '')
                        a.issueTracker = app.get('issueTracker', '')
                        a.changelog = app.get('changelog', '')

                        a.donate = app.get('donate', '')
                        a.bitcoin = app.get('bitcoin', '')
                        a.litecoin = app.get('litecoin', '')
                        a.flattrID = app.get('flattrID', '')
                        a.liberapayID = app.get('liberapayID', '')

                        a.added = datetime.datetime.fromtimestamp(int(app.get('added', 0))/1000., datetime.timezone.utc)
                        a.lastUpdated = datetime.datetime.fromtimestamp(int(app.get('lastUpdated', 0))/1000., datetime.timezone.utc)

                        a.icon = app.get('icon', '')
                        a.site = fdroid_site

                        # we're not importing categories and antiFeatures at the moment
                        #
                        #a.categories.clear()
                        #for category in app.get('categories', []):
                        #    a.categories.add(get_or_create_category(html.unescape(category)))
                        #a.antiFeatures.clear()
                        #for antiFeature in app.get('antiFeatures', []):
                        #    a.antiFeatures.add(get_or_create_antifeature(html.unescape(antiFeature)))
                        # a.save()

                    App.objects.bulk_update(batch_update, ['name', 'packageName', 'summary', 'description', 'lastUpdated'])
                    App.objects.bulk_create(batch_create)

                for appbatch in batch(index['apps'], 50):
                    batch_create = collections.defaultdict(list)
                    batch_update = []
                    for app_info in appbatch:
                        for langName, loc in app_info.get('localized', {}).items():
                            l = get_or_create_lang(fdroid_website_search_tags._map_lang(langName))
                            app = App.objects.get(packageName=app_info['packageName'])
                            aI18n = None
                            for x in batch_create[app.packageName]:
                                if x.entry == app and x.lang == l:
                                    aI18n = x
                            if not aI18n:
                                aI18n = AppI18n.objects.filter(entry=app, lang=l).first()
                                if aI18n:
                                    batch_update.append(aI18n)
                            if not aI18n:
                                aI18n = AppI18n()
                                aI18n.entry = app
                                aI18n.lang = l
                                batch_create[app.packageName].append(aI18n)
                            if 'name' in loc:
                                aI18n.name = html.unescape(loc['name'])
                            if 'summary' in loc:
                                aI18n.summary = html.unescape(loc['summary'])
                            if 'description' in loc:
                                aI18n.description = html.unescape(loc['description'])
                            if 'whatsNew' in loc:
                                aI18n.whatsNew = html.unescape(loc['whatsNew'])
                            if 'icon' in loc:
                                aI18n.icon = '/' + langName + '/' + html.unescape(loc['icon'])
                    AppI18n.objects.bulk_update(batch_update, ['name', 'summary', 'description'])
                    AppI18n.objects.bulk_create(itertools.chain(*batch_create.values()))

            print("Parsed infos of {} Apps and stored them in the local database.".format(len(index['apps'])))
        print("There are infos about {} Apps stored in the local database now.".format(App.objects.count()))

        transaction.commit()
        with connection.cursor() as cursor:
            cursor.execute("DROP TABLE IF EXISTS appfts;")
            cursor.execute("CREATE VIRTUAL TABLE appfts USING FTS5(names, summaries, descriptions, packageName, appid UNINDEXED, tokenize='unicode61');")
            cursor.execute("INSERT INTO appfts(appfts, rank) VALUES('rank', 'bm25(100.0, 10.0, 1.0, 10.0)');")
        transaction.commit()
        with connection.cursor() as cursor:
            cursor.execute(textwrap.dedent("""\
                INSERT INTO appfts(names, summaries, descriptions, packageName, appid)
                SELECT
                    fdroid_website_search_app.name || ' ' || group_concat(fdroid_website_search_appi18n.name, ' ')
                    || ' ' || replace(fdroid_website_search_app.name, '-', '') || ' ' || replace(group_concat(fdroid_website_search_appi18n.name, ' '), '-', ''),
                    fdroid_website_search_app.summary || ' ' || group_concat(fdroid_website_search_appi18n.summary, ' '),
                    fdroid_website_search_app.description || ' ' || group_concat(fdroid_website_search_appi18n.description, ' '),
                    fdroid_website_search_app.packageName,
                    fdroid_website_search_app.id
                FROM fdroid_website_search_appi18n
                INNER JOIN fdroid_website_search_app
                ON fdroid_website_search_app.id=fdroid_website_search_appi18n.entry_id
                GROUP BY fdroid_website_search_app.id;
                """))
        transaction.commit()
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO appfts(appfts) VALUES('optimize');")
            cursor.execute("VACUUM;")
        transaction.commit()
        # cursor.execute("SELECT foo FROM bar WHERE baz = %s", [self.baz])
        # row = cursor.fetchone()
