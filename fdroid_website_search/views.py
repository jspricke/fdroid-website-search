from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from django.db.models import Q
from django.core.paginator import Paginator

from .models import App, AppI18n, Language, FDroidSite, FTSResult

from .templatetags import fdroid_website_search_tags

import html


class FDroidSearchView(View):
    def get(self, request):
        querystr = html.escape(html.unescape(request.GET.get("q", "")))
        pagenum = request.GET.get("page", 1)

        lang = fdroid_website_search_tags._map_lang(self.request.GET.get("lang", ""))
        if lang:
            try:
                Language.objects.get(name=lang)
            except Language.DoesNotExist:
                pass

        r = []
        if querystr:
            r = FTSResult.objects.raw(
                "SELECT appid as app_id, rank FROM appfts WHERE appfts MATCH %s ORDER BY RANK;",
                params=['"' + querystr + '"'],
            )

        p = Paginator([{"object": x.app} for x in r], 20)
        page_obj = p.get_page(pagenum)
        return render(
            request,
            "search/search.html",
            {"page_obj": page_obj, "lang": lang, "query": querystr},
        )


def config_info(req):

    d = {}

    d["repos"] = []
    for site in FDroidSite.objects.all():
        d["repos"].append(
            {
                "last_updated": site.lastUpdated.isoformat(),
                "repo_timestamp": site.repoTimestamp.isoformat(),
                "site_url": site.siteUrl,
                "repo_url": site.repoUrl,
                "icon_mirror_url": site.iconMirrorUrl,
            }
        )

    return JsonResponse(d)
