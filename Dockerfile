FROM debian:buster

RUN printf "path-exclude=/usr/share/locale/*\npath-exclude=/usr/share/man/*\npath-exclude=/usr/share/doc/*\npath-include=/usr/share/doc/*/copyright\n" >/etc/dpkg/dpkg.cfg.d/01_nodoc \
	&& apt-get update \
	&& apt-get -y upgrade \
	&& apt-get -y dist-upgrade \
	&& apt-get install -y --no-install-recommends \
		python3 \
		python3-pip \
		python3-setuptools \
	&& apt-get -y autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

ADD manage.py /opt/fdroid-website-search/manage.py
ADD requirements.txt /opt/fdroid-website-search/requirements.txt
ADD fdroid_website_search /opt/fdroid-website-search/fdroid_website_search
ADD docker_srv_settings.py /opt/fdroid-website-search/srv_settings.py
ADD start_in_docker.sh /opt/fdroid-website-search/start_in_docker.sh

WORKDIR /opt/fdroid-website-search

RUN python3 -m pip install -r requirements.txt
RUN python3 -m pip install 'gunicorn<20.0'
RUN python3 manage.py migrate
RUN python3 manage.py collectstatic --no-input
RUN python3 manage.py fdroidfetchindex
RUN chgrp -R nogroup /opt/fdroid-website-search && \
    chmod -R g+rX /opt/fdroid-website-search

EXPOSE 8000
USER nobody
CMD /bin/bash start_in_docker.sh
